Feature: Login functionality to be check

Scenario Outline: Check login is successful with valid credentials

Given user is on login page

When user enters username "<USERNAME>" in email field

And user enter password "<PASSWORD>" in password field

Then click on login button

    Examples: 
      | USERNAME                   |  PASSWORD    |
      | aadityapatil@gmail.com     |  Aaditya@12  | 
      | demo2023@demo.com          |  Demo@123    | 
