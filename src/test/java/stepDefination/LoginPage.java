package stepDefination;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginPage {
	
	WebDriver driver;
	
	@Given("^user is on login page$")
	public void user_is_on_login_page() {
	   
		driver= new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/login");
		driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}

	@When("user enters username {string} in email field")
	public void user_enters_username_in_email_field(String string) {
	    
		driver.findElement(By.id("Email")).sendKeys(string);
	}

	@When("user enter password {string} in password field")
	public void user_enter_password_in_password_field(String string) {
		
		driver.findElement(By.id("Password")).sendKeys(string);
	   
	}

	@Then("click on login button")
	public void click_on_login_button() {
		
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	    
	}

}
